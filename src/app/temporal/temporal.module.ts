import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TemporalComponent } from './temporal.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [TemporalComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [TemporalComponent]
})
export class TemporalModule {
}
