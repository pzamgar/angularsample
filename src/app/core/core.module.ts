import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found/not-found.component';
import { HeaderComponent } from './shell/header/header.component';
import { ShellComponent } from './shell/shell.component';
import { MainComponent } from './shell/main/main.component';
import { FooterComponent } from './shell/footer/footer.component';
import { RouterModule } from '@angular/router';
import { TemporalModule } from '../temporal/temporal.module';
import { ContactsModule } from '../contacts/contacts.module';


@NgModule({
  declarations: [
    ShellComponent,
    NotFoundComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    TemporalModule,
    ContactsModule
  ],
  exports: [ShellComponent]
})
export class CoreModule {
}
